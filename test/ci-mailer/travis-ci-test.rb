# Copyright (C) 2018-2019  Sutou Kouhei <kou@clear-code.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

class CIMailerTravisCITest < Test::Unit::TestCase
  include Helper

  def setup
    options = YAML.load_file(fixture_path("config-multi-site.yaml"))
    @boundary = "boundary"
    options["boundary"] = @boundary
    @ci_mailer_inputs = []
    options["ci_mailer_stub"] = lambda do |*inputs|
      @ci_mailer_inputs << inputs
    end
    @options = WebhookMailer::Options.new(options)
  end

  def test_passed
    raw_payload = {
      "state" => "passed",
      "status" => 0,
      "status_message" => "Fixed",
      "build_url" => "https://travis-ci.org/groonga/groonga/builds/415764765",
      "started_at" => "2018-08-14T04:36:09Z",
      "finished_at" => "2018-08-14T06:16:09Z",
      "author_name" => "Kouhei Sutou",
      "committed_at" => "2018-08-14T05:17:35Z",
      "branch" => "master",
      "commit" => "51e34b2ac6b9c61c06d5ad68dd56aa72360d35c6",
      "message" => "Fix a bug",
      "compare_url" => "https://github.com/groonga/groonga/compare/4544544121e3...51e34b2ac6b9",
      "repository" => {
        "name" => "groonga",
        "owner_name" => "groonga",
      },
      "matrix" => [
        {
          "id" => 415764766,
          "number" => "10375.1",
          "state" => "passed",
          "started_at" => "2018-08-14T06:05:01Z",
          "finished_at" => "2018-08-14T06:14:09Z",
          "config" => {
            "os" => "linux",
            "language" => "c",
          },
        },
        {
          "id" => 415764767,
          "number" => "10375.2",
          "state" => "passed",
          "started_at" => "2018-08-14T06:01:23Z",
          "finished_at" => "2018-08-14T06:01:25Z",
          "config" => {
            "os" => "linux",
            "language" => "c",
          },
        },
      ],
    }
    payload = WebhookMailer::Payload.new(raw_payload)
    mailer = WebhookMailer::CIMailer.new({
                                           "status" => "failed",
                                           "label" => "Failed",
                                         },
                                         payload,
                                         @options)
    mailer.send_email
    mail = <<-MAIL.gsub(/\n/, "\r\n")
X-Mailer: WebhookMailer/#{WebhookMailer::VERSION}
MIME-Version: 1.0
Content-Type: multipart/alternative;
 boundary=#{@boundary}
From: Travis CI <sender@example.com>
Sender: sender@example.com
To: global-to@example.com
Subject: groonga/groonga Fixed [master] 51e34b2a
Date: Tue, 14 Aug 2018 06:16:09 -0000

--#{@boundary}
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit

Travis CI: failed -> Fixed
  https://travis-ci.org/groonga/groonga/builds/415764765

Builds:
  10375.1: passed: 9m8s:
    https://travis-ci.org/groonga/groonga/jobs/415764766
    OS: linux
    Language: c
  10375.2: passed: 2s:
    https://travis-ci.org/groonga/groonga/jobs/415764767
    OS: linux
    Language: c

Commit author:
  Kouhei Sutou
Commit date:
  2018-08-14T05:17:35Z
Commit ID:
  51e34b2ac6b9c61c06d5ad68dd56aa72360d35c6
Commit changes:
  https://github.com/groonga/groonga/compare/4544544121e3...51e34b2ac6b9
Commit message:
  Fix a bug

--#{@boundary}
Content-Type: text/html; charset=utf-8
Content-Transfer-Encoding: 8bit

<!DOCTYPE html>
<html>
  <head>
  </head>
  <body>
    <h1>
      Travis CI:
      failed &rarr;
      <a href="https://travis-ci.org/groonga/groonga/builds/415764765">
        <span style="background-color: #a6f3a6; color: #000000">Fixed</span>
      </a>
    </h1>
    <h2>Builds</h2>
    <table>
      <thead>
        <tr>
          <th>Status</th>
          <th>Elapsed</th>
          <th>Name</th>
          <th>Language</th>
          <th>OS</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td><a href="https://travis-ci.org/groonga/groonga/jobs/415764766"><span style="background-color: #a6f3a6; color: #000000">passed</span></a></td>
          <td>9m8s</td>
          <td>10375.1</td>
          <td>c</td>
          <td>linux</td>
        </tr>
        <tr>
          <td><a href="https://travis-ci.org/groonga/groonga/jobs/415764767"><span style="background-color: #a6f3a6; color: #000000">passed</span></a></td>
          <td>2s</td>
          <td>10375.2</td>
          <td>c</td>
          <td>linux</td>
        </tr>
      </tbody>
    </table>
    <h2>Commit</h2>
    <dl style="line-height: 1.5; margin-left: 2em">
      <dt style="clear: both; float: left; font-weight: bold; width: 8em">Author</dt>
      <dd style="margin-left: 8.5em">Kouhei Sutou</dd>
      <dt style="clear: both; float: left; font-weight: bold; width: 8em">Date</dt>
      <dd style="margin-left: 8.5em">2018-08-14T05:17:35Z</dd>
      <dt style="clear: both; float: left; font-weight: bold; width: 8em">New revision</dt>
      <dd style="margin-left: 8.5em"><a href="https://github.com/groonga/groonga/compare/4544544121e3...51e34b2ac6b9">51e34b2ac6b9c61c06d5ad68dd56aa72360d35c6</a></dd>
      <dt style="clear: both; float: left; font-weight: bold; width: 8em">Message</dt>
      <dd style="margin-left: 8.5em"><pre style="border: 1px solid #aaa; font-family: Consolas, Menlo, &quot;Liberation Mono&quot;, Courier, monospace; line-height: 1.2; padding: 0.5em; width: auto">Fix a bug</pre></dd>
    </dl>
  </body>
</html>

--#{@boundary}--
    MAIL
    assert_equal([
                   [
                     "localhost",
                     25,
                     "sender@example.com",
                     ["global-to@example.com"],
                     mail,
                   ]
                 ],
                 @ci_mailer_inputs)
  end

  def test_pull_request
    raw_payload = {
      "state" => "passed",
      "status" => 0,
      "status_message" => "Passed",
      "build_url" => "https://travis-ci.org/red-data-tools/red-datasets/builds/447061204",
      "started_at" => "2018-10-27T11:55:58Z",
      "finished_at" => "2018-10-27T11:58:31Z",
      "author_name" => "Kouhei Sutou",
      "committed_at" => "2018-10-27T11:55:06Z",
      "branch" => "master",
      "commit" => "75b1bb799b16e8c0e4be4c91c681a43fca8555c8",
      "message" => "Fix test name",
      "compare_url" => "https://github.com/red-data-tools/red-datasets/pull/33",
      "pull_request" => true,
      "pull_request_number" => 33,
      "repository" => {
        "name" => "red-datasets",
        "owner_name" => "red-data-tools",
      },
      "matrix" => [
        {
          "id" => 447061205,
          "number" => "166.1",
          "state" => "passed",
          "config" => {
            "os" => "linux",
            "rvm" => "2.3.7",
            "name" => "Ruby 2.3",
            "dist" => "trusty",
            "cache" => {
              "directories" => ["$HOME/.cache/red-datasets/"]
            },
            "group" => "stable",
            ".result" => "configured",
            "language" => "ruby",
            "notifications" => {
              "webhooks" => ["https://webhook.commit-email.info/"],
            },
          },
          "started_at" => "2018-10-27T11:55:58Z",
          "finished_at" => "2018-10-27T11:58:31Z",
          "allow_failure" => false,
        },
        {
          "id" => 447061206,
          "number" => "166.2",
          "state" => "passed",
          "config" => {
            "os" => "linux",
            "rvm" => "2.4.4",
            "name" => "Ruby 2.4",
            "dist" => "trusty",
            "cache" => {
              "directories" => ["$HOME/.cache/red-datasets/"],
            },
            "group" => "stable",
            ".result" => "configured",
            "language" => "ruby",
            "notifications" => {
              "webhooks" => ["https://webhook.commit-email.info/"],
            }
          },
          "started_at" => "2018-10-27T11:55:58Z",
          "finished_at" => "2018-10-27T11:58:31Z",
          "allow_failure" => false,
        },
        {
          "id" => 447061207,
          "number" => "166.3",
          "state" => "passed",
          "config" =>  {
            "os" => "linux",
            "rvm" => "2.5.1",
            "name" => "Ruby 2.5",
            "dist" => "trusty",
            "cache" => {
              "directories" => ["$HOME/.cache/red-datasets/"],
            },
            "group" => "stable",
            ".result" => "configured",
            "language" => "ruby",
            "notifications" => {
              "webhooks" => ["https://webhook.commit-email.info/"],
            },
          },
          "started_at" => "2018-10-27T11:55:58Z",
          "finished_at" => "2018-10-27T11:58:31Z",
          "allow_failure" => false,
        },
        {
          "id" => 447061208,
          "number" => "166.4",
          "state" => "passed",
          "config" => {
            "os" => "linux",
            "rvm" => "ruby-head",
            "name" => "Ruby master",
            "dist" => "trusty",
            "cache" => {
              "directories" => ["$HOME/.cache/red-datasets/"],
            },
            "group" => "stable",
            ".result" => "configured",
            "language" => "ruby",
            "notifications" => {
              "webhooks" => ["https://webhook.commit-email.info/"],
            },
          },
          "started_at" => "2018-10-27T11:55:58Z",
          "finished_at" => "2018-10-27T11:58:31Z",
          "allow_failure" => false,
        },
      ],
    }
    payload = WebhookMailer::Payload.new(raw_payload)
    mailer = WebhookMailer::CIMailer.new({
                                           "status" => "failed",
                                           "label" => "Failed",
                                         },
                                         payload,
                                         @options)
    mailer.send_email
    mail = <<-MAIL.gsub(/\n/, "\r\n")
X-Mailer: WebhookMailer/#{WebhookMailer::VERSION}
MIME-Version: 1.0
Content-Type: multipart/alternative;
 boundary=#{@boundary}
From: Travis CI <sender@example.com>
Sender: sender@example.com
To: global-to@example.com
Subject: red-data-tools/red-datasets Passed [PR#33] 75b1bb79
Date: Sat, 27 Oct 2018 11:58:31 -0000

--#{@boundary}
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit

Travis CI: failed -> Passed
  https://travis-ci.org/red-data-tools/red-datasets/builds/447061204

Builds:
  Ruby 2.3: passed: 2m33s:
    https://travis-ci.org/red-data-tools/red-datasets/jobs/447061205
  Ruby 2.4: passed: 2m33s:
    https://travis-ci.org/red-data-tools/red-datasets/jobs/447061206
  Ruby 2.5: passed: 2m33s:
    https://travis-ci.org/red-data-tools/red-datasets/jobs/447061207
  Ruby master: passed: 2m33s:
    https://travis-ci.org/red-data-tools/red-datasets/jobs/447061208

Commit author:
  Kouhei Sutou
Commit date:
  2018-10-27T11:55:06Z
Commit ID:
  75b1bb799b16e8c0e4be4c91c681a43fca8555c8
Commit changes:
  https://github.com/red-data-tools/red-datasets/pull/33
Commit message:
  Fix test name

--#{@boundary}
Content-Type: text/html; charset=utf-8
Content-Transfer-Encoding: 8bit

<!DOCTYPE html>
<html>
  <head>
  </head>
  <body>
    <h1>
      Travis CI:
      failed &rarr;
      <a href="https://travis-ci.org/red-data-tools/red-datasets/builds/447061204">
        <span style="background-color: #a6f3a6; color: #000000">Passed</span>
      </a>
    </h1>
    <h2>Builds</h2>
    <table>
      <thead>
        <tr>
          <th>Status</th>
          <th>Elapsed</th>
          <th>Name</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td><a href="https://travis-ci.org/red-data-tools/red-datasets/jobs/447061205"><span style="background-color: #a6f3a6; color: #000000">passed</span></a></td>
          <td>2m33s</td>
          <td>Ruby 2.3</td>
        </tr>
        <tr>
          <td><a href="https://travis-ci.org/red-data-tools/red-datasets/jobs/447061206"><span style="background-color: #a6f3a6; color: #000000">passed</span></a></td>
          <td>2m33s</td>
          <td>Ruby 2.4</td>
        </tr>
        <tr>
          <td><a href="https://travis-ci.org/red-data-tools/red-datasets/jobs/447061207"><span style="background-color: #a6f3a6; color: #000000">passed</span></a></td>
          <td>2m33s</td>
          <td>Ruby 2.5</td>
        </tr>
        <tr>
          <td><a href="https://travis-ci.org/red-data-tools/red-datasets/jobs/447061208"><span style="background-color: #a6f3a6; color: #000000">passed</span></a></td>
          <td>2m33s</td>
          <td>Ruby master</td>
        </tr>
      </tbody>
    </table>
    <h2>Commit</h2>
    <dl style="line-height: 1.5; margin-left: 2em">
      <dt style="clear: both; float: left; font-weight: bold; width: 8em">Author</dt>
      <dd style="margin-left: 8.5em">Kouhei Sutou</dd>
      <dt style="clear: both; float: left; font-weight: bold; width: 8em">Date</dt>
      <dd style="margin-left: 8.5em">2018-10-27T11:55:06Z</dd>
      <dt style="clear: both; float: left; font-weight: bold; width: 8em">New revision</dt>
      <dd style="margin-left: 8.5em"><a href="https://github.com/red-data-tools/red-datasets/pull/33">75b1bb799b16e8c0e4be4c91c681a43fca8555c8</a></dd>
      <dt style="clear: both; float: left; font-weight: bold; width: 8em">Message</dt>
      <dd style="margin-left: 8.5em"><pre style="border: 1px solid #aaa; font-family: Consolas, Menlo, &quot;Liberation Mono&quot;, Courier, monospace; line-height: 1.2; padding: 0.5em; width: auto">Fix test name</pre></dd>
    </dl>
  </body>
</html>

--#{@boundary}--
    MAIL
    assert_equal([
                   [
                     "localhost",
                     25,
                     "sender@example.com",
                     ["global-to@example.com"],
                     mail,
                   ]
                 ],
                 @ci_mailer_inputs)
  end
end
