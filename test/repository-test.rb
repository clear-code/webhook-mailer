# Copyright (C) 2019  Sutou Kouhei <kou@clear-code.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

class RepositoryTest < Test::Unit::TestCase
  include Helper

  class StubPayload < Struct.new(:repository_domain,
                                 :repository_owner,
                                 :repository_name,
                                 :event_type)
  end

  def setup
    @payload = StubPayload.new
    @payload.repository_domain = "gitlab.com"
    @payload.repository_owner = "clear-code"
    @payload.repository_name = "webhook-mailer"
    options = YAML.load_file(fixture_path("config-multi-site.yaml"))
    @options = WebhookMailer::Options.new(options)
  end

  sub_test_case("#target_reference?") do
    def setup
      super
      @repository_options = @options.dig("domains",
                                         "gitlab.com",
                                         "owners",
                                         "clear-code",
                                         "repositories",
                                         "webhook-mailer")
    end

    def test_nil
      repository = WebhookMailer::Repository.new(@payload, @options)
      assert do
        repository.target_reference?("refs/heads/master")
      end
    end

    def test_empty
      @repository_options["references"] = []
      repository = WebhookMailer::Repository.new(@payload, @options)
      assert do
        repository.target_reference?("refs/heads/master")
      end
    end

    def test_no_match
      @repository_options["references"] = ["refs/heads/develop"]
      repository = WebhookMailer::Repository.new(@payload, @options)
      assert do
        not repository.target_reference?("refs/heads/master")
      end
    end

    def test_not_match
      @repository_options["references"] = [
        "!refs/heads/develop",
        "refs/heads/develop",
      ]
      repository = WebhookMailer::Repository.new(@payload, @options)
      assert do
        not repository.target_reference?("refs/heads/develop")
      end
    end

    def test_match
      @repository_options["references"] = [
        "refs/heads/master",
        "refs/heads/develop",
        "!refs/heads/*",
      ]
      repository = WebhookMailer::Repository.new(@payload, @options)
      assert do
        repository.target_reference?("refs/heads/develop")
      end
    end

    def test_glob
      @repository_options["references"] = [
        "**/*",
      ]
      repository = WebhookMailer::Repository.new(@payload, @options)
      assert do
        repository.target_reference?("refs/heads/master")
      end
    end
  end
end
