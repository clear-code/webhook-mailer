# Copyright (C) 2010-2019  Sutou Kouhei <kou@clear-code.com>
# Copyright (C) 2015  Kenji Okimoto <okimoto@clear-code.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

module WebhookMailer
  class Repository
    include PathResolver

    attr_reader :domain
    attr_reader :owner
    attr_reader :name
    def initialize(payload, options)
      @domain = payload.repository_domain
      @owner = payload.repository_owner
      @name = payload.repository_name
      if @domain.nil?
        raise UnexpectedPayloadError.new("repository domain is missing", payload)
      elsif @owner.nil?
        raise UnexpectedPayloadError.new("repository owner is missing", payload)
      elsif @name.nil?
        raise UnexpectedPayloadError.new("repository name is missing", payload)
      end
      @payload = payload
      @options = options.repository_options(@payload)
      @to = @options[:to]
      @max_n_retries = (@options[:n_retries] || 3).to_i
      @use_ssh = @options[:use_ssh]
      raise Error.new("mail receive address is missing: <#{@name}>") if @to.nil?
    end

    def enabled?
      enabled = @options[:enabled]
      enabled = true if enabled.nil?
      enabled
    end

    def target_reference?(reference)
      target_reference_patterns = @options[:references] || []
      return true if target_reference_patterns.empty?
      target_reference_patterns.each do |pattern|
        if pattern.start_with?("!")
          return false if match_reference?(pattern[1..-1], reference)
        else
          return true if match_reference?(pattern, reference)
        end
      end
      false
    end

    def process_commit(before, after, reference)
      FileUtils.mkdir_p(File.dirname(mirror_path))
      n_retries = 0
      lock("#{mirror_path}.lock") do
        begin
          if File.exist?(mirror_path)
            git("--git-dir", mirror_path, "fetch", "--quiet", "--prune")
          else
            if @use_ssh
              clone_url = @payload.ssh_clone_url
            else
              clone_url = @payload.http_clone_url
            end
            git("clone", "--quiet",
                "--mirror", clone_url,
                mirror_path)
          end
        rescue Error
          n_retries += 1
          retry if n_retries <= @max_n_retries
          raise
        end
      end
      send_commit_email(before, after, reference)
    end

    def process_ci
      FileUtils.mkdir_p(File.dirname(mirror_path))
      state = {}
      state_path = "#{mirror_path}.#{@payload.service_id}.json"
      lock("#{mirror_path}.lock") do
        if File.exist?(state_path)
          begin
            state = JSON.parse(File.read(state_path))
          rescue JSON::ParserError
          end
        end
        File.open(state_path, "w") do |state_file|
          state_file.puts(JSON.pretty_generate(generate_ci_state))
        end
      end
      if !@payload.success? or @payload.status != state["status"]
        send_ci_email(state)
      end
    end

    private
    def match_reference?(pattern, reference)
      File.fnmatch(pattern, reference, File::FNM_PATHNAME | File::FNM_EXTGLOB)
    end

    def lock(path)
      File.open(path, "w") do |file|
        file.flock(File::LOCK_EX)
        yield
      end
    end

    def send_commit_email(before, after, reference)
      options = [
        "--repository", mirror_path,
        "--max-size", "1M"
      ]
      if @payload.gitlab?
        if @payload.gitlab_wiki?
          add_option(options, "--repository-browser", "gitlab-wiki")
        else
          add_option(options, "--repository-browser", "gitlab")
        end
        add_option(options, "--gitlab-project-uri", @payload.web_url)
      else
        if @payload.github_gollum?
          add_option(options, "--repository-browser", "github-wiki")
        else
          add_option(options, "--repository-browser", "github")
        end
        add_option(options, "--github-user", @owner)
        add_option(options, "--github-repository", @name)
      end
      name = "#{@owner}/#{@name}"
      name << ".wiki" if @payload.wiki?
      add_option(options, "--name", name)
      add_option(options, "--from", from)
      add_option(options, "--from-domain", from_domain)
      add_option(options, "--sender", sender)
      add_option(options, "--sleep-per-mail", sleep_per_mail)
      options << "--send-per-to" if send_per_to?
      options << "--add-html" if add_html?
      error_to.each do |_error_to|
        options.concat(["--error-to", _error_to])
      end
      if @to.is_a?(Array)
        options.concat(@to)
      else
        options << @to
      end
      stub = @options[:commit_mailer_stub]
      if stub.respond_to?(:call)
        stub.call(options, before, after, reference)
      else
        begin
          mailer = GitCommitMailer.parse_options_and_create(options)
          mailer.process_reference_change(before, after, reference)
          mailer.send_all_mails
        rescue => error
          message = "failed to run git-commit-mailer: <"
          message << options.join(" ")
          message << ">:<#{before}>:<#{after}>:<#{reference}>:\n"
          message << "#{error.class}: #{error.message}"
          raise Error.new(message)
        end
      end
    end

    def git(*arguments)
      arguments = arguments.collect {|argument| argument.to_s}
      command_line = [git_command, *arguments]
      unless system(*command_line)
        raise Error.new("failed to run command: <#{command_line.join(' ')}>")
      end
    end

    def git_command
      @git ||= @options[:git] || "git"
    end

    def mirrors_directory
      @mirrors_directory ||=
        @options[:mirrors_directory] ||
        path("mirrors")
    end

    def mirror_path
      components = [mirrors_directory, @domain, @owner]
      if @payload.github_gollum? or @payload.gitlab_wiki?
        components << "#{@name}.wiki"
      else
        components << @name
      end
      File.join(*components)
    end

    def from
      @from ||= @options[:from]
    end

    def from_domain
      @from_domain ||= @options[:from_domain]
    end

    def sender
      @sender ||= @options[:sender]
    end

    def sleep_per_mail
      @sleep_per_mail ||= @options[:sleep_per_mail]
    end

    def error_to
      @error_to ||= force_array(@options[:error_to])
    end

    def send_per_to?
      @options[:send_per_to]
    end

    def add_html?
      @options[:add_html]
    end

    def force_array(value)
      if value.is_a?(Array)
        value
      elsif value.nil?
        []
      else
        [value]
      end
    end

    def add_option(options, name, value)
      return if value.nil?
      value = value.to_s
      return if value.empty?
      options.concat([name, value])
    end

    def send_ci_email(previous_state)
      mailer = CIMailer.new(previous_state, @payload, @options)
      mailer.send_email
    end

    def generate_ci_state
      {
        "status" => @payload.status,
        "status_label" => @payload.status_label,
      }
    end
  end
end
