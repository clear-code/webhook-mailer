# Copyright (C) 2010-2018  Kouhei Sutou <kou@clear-code.com>
# Copyright (C) 2015  Kenji Okimoto <okimoto@clear-code.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

module WebhookMailer
  class Options
    include Enumerable

    def initialize(options)
      @options = options
    end

    def [](key)
      @options[key.to_s]
    end

    def dig(*keys)
      @options.dig(*keys.collect(&:to_s))
    end

    def repository_options(payload)
      domain = payload.repository_domain
      owner = payload.repository_owner
      name = payload.repository_name
      event_type = payload.event_type

      domain_options = dig(:domains, domain) || {}
      domain_owner_options = dig(:domains, domain,
                                 :owners, owner) || {}
      domain_repository_options = dig(:domains, domain,
                                      :owners, owner,
                                      :repositories, name) || {}
      if event_type
        domain_repository_event_type_options = dig(:domains, domain,
                                                   :owners, owner,
                                                   :repositories, name,
                                                   event_type)
      end
      domain_repository_event_type_options ||= {}

      owner_options = dig(:owners, owner) || {}
      owner_repository_options = dig(:owners, owner,
                                     :repositories, name) || {}
      if event_type
        owner_repository_event_type_options = dig(:owners, owner,
                                                  :repositories, name,
                                                  event_type)
      end
      owner_repository_event_type_options ||= {}

      options = @options.merge(owner_options)
      options = options.merge(owner_repository_options)
      options = options.merge(owner_repository_event_type_options)
      options = options.merge(domain_options)
      options = options.merge(domain_owner_options)
      options = options.merge(domain_repository_options)
      options = options.merge(domain_repository_event_type_options)
      self.class.new(options)
    end
  end
end
