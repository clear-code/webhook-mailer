# Copyright (C) 2010-2019  Sutou Kouhei <kou@clear-code.com>
# Copyright (C) 2015  Kenji Okimoto <okimoto@clear-code.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

require "octokit"

module WebhookMailer
  class Payload
    attr_reader :data

    def initialize(data, metadata={})
      @data = data
      @metadata = metadata
    end

    def [](key)
      key.split(".").inject(@data) do |current_data, current_key|
        if current_data
          current_data[current_key]
        else
          nil
        end
      end
    end

    def http_clone_url
      if gitlab_wiki?
        self["wiki.git_http_url"]
      elsif gitlab?
        self["repository.git_http_url"]
      elsif github_gollum?
        self["repository.clone_url"].gsub(/(\.git)\z/, ".wiki\\1")
      else
        self["repository.clone_url"]
      end
    end

    def ssh_clone_url
      if gitlab_wiki?
        self["wiki.git_ssh_url"]
      elsif gitlab?
        self["repository.git_ssh_url"]
      elsif github_gollum?
        self["repository.ssh_url"].gsub(/(\.git)\z/, ".wiki\\1")
      else
        self["repository.ssh_url"]
      end
    end

    def gitlab?
      not self["object_kind"].nil?
    end

    def gitlab_ci?
      gitlab? and self["object_kind"] == "pipeline"
    end

    def gitlab_wiki?
      gitlab? and self["object_kind"] == "wiki_page"
    end

    def github?
      not @metadata["x-github-event"].nil?
    end

    def github_gollum?
      github? and @metadata["x-github-event"] == "gollum"
    end

    def github_actions?
      github? and @metadata["x-github-event"] == "check_suite"
    end

    def wiki?
      gitlab_wiki? or github_gollum?
    end

    def travis_ci?
      (self["build_url"] || "").start_with?("https://travis-ci.org/")
    end

    def appveyor?
      not self["eventName"].nil?
    end

    def label
      # TODO: GitHub Actions doesn't provide REST API to get workflow name
      # for now. We can enable the following logic when GitHub Actions provides
      # it.
      #
      # if github_actions?
      #   "#{service_name}: #{workflow_name_github_actions}"
      # else
      #   service_name
      # end
      service_name
    end

    def service_name
      if travis_ci?
        "Travis CI"
      elsif appveyor?
        "AppVeyor"
      elsif gitlab_ci?
        "GitLab CI"
      elsif gitlab?
        "GitLab"
      elsif github_actions?
        "GitHub Actions"
      else
        "GitHub"
      end
    end

    def service_id
      if travis_ci?
        "travis-ci"
      elsif appveyor?
        "appveyor"
      elsif gitlab_ci?
        "gitlab-ci"
      elsif gitlab?
        "gitlab"
      elsif github_actions?
        "github-actions"
      else
        "github"
      end
    end

    def event_name
      if travis_ci? or appveyor? or gitlab_ci? or github_actions?
        "ci"
      elsif gitlab?
        self["object_kind"]
      elsif github?
        @metadata["x-github-event"]
      else
        nil
      end
    end

    def event_type
      case event_name
      when "ci"
        :ci
      when nil
        nil
      else
        :diff
      end
    end

    def web_url
      if travis_ci?
        self["build_url"]
      elsif appveyor?
        self["eventData.buildUrl"]
      elsif gitlab_ci?
        project_web_url = self["project.web_url"]
        id = self["object_attributes.id"]
        "#{project_web_url}/pipelines/#{id}"
      elsif gitlab?
        self["project.web_url"]
      elsif github_actions?
        commit = self["check_suite.head_sha"]
        check_suite_id = self["check_suite.id"]
        self["repository.html_url"] +
          "/commit/#{commit}/checks?check_suite_id=#{check_suite_id}"
      else
        self["repository.html_url"]
      end
    end

    def repository_domain
      if travis_ci?
        "github.com"
      elsif appveyor?
        begin
          URI.parse(self["eventData.commitUrl"]).hostname
        rescue URI::InvalidURIError
          nil
        end
      else
        begin
          URI.parse(web_url).hostname
        rescue URI::InvalidURIError
          nil
        end
      end
    end

    def repository_name
      if appveyor?
        self["eventData.repositoryName"].split("/")[1]
      elsif gitlab_ci?
        project_web_url = self["project.web_url"]
        begin
          URI.parse(project_web_url).path.split("/").last
        rescue URI::InvalidURIError
          nil
        end
      elsif gitlab?
        begin
          URI.parse(web_url).path.split("/").last
        rescue URI::InvalidURIError
          nil
        end
      else
        self["repository.name"]
      end
    end

    def repository_owner
      if travis_ci?
        self["repository.owner_name"]
      elsif appveyor?
        self["eventData.repositoryName"].split("/")[0]
      elsif gitlab?
        begin
          URI.parse(web_url).path.split("/")[1]
        rescue URI::InvalidURIError
          nil
        end
      else
        owner = self["repository.owner"] || {}
        owner["name"] || owner["login"]
      end
    end

    def finished_at
      if travis_ci?
        iso8601_value(self["finished_at"])
      elsif appveyor?
        appveyor_time_value(self["eventData.finished"])
      elsif gitlab?
        time_value(self["object_attributes.finished_at"])
      elsif github_actions?
        iso8601_value(self["check_suite.updated_at"])
      else
        nil
      end
    end

    def finished?
      if gitlab_ci?
        # Payload from GitLab CI may have "finished_at" even when
        # "status" is "running". It seems that this is a GitLab CI
        # bug. It may be caused when the target pipeline is finished
        # while the "running" payload is generating.
        return false if status == "running"
      elsif github_actions?
        # Payload from GitHub Actions may have "finished_at" even when
        # "status" is "action_required". It seems that this is a
        # GitHub Actions bug. It may be caused when the target
        # workflow is finished while the "action_required" payload is
        # generating.
        return false if status == "action_required"
      end

      if travis_ci? or appveyor? or gitlab?
        not finished_at.nil?
      else
        true
      end
    end

    def success?
      if travis_ci?
        self["status"].zero?
      elsif appveyor?
        self["eventData.passed"]
      elsif gitlab?
        self["object_attributes.status"] == "success"
      elsif github_actions?
        self["check_suite.conclusion"] == "success"
      else
        true
      end
    end

    def status
      if travis_ci?
        self["state"]
      elsif appveyor?
        self["eventName"].gsub(/\Abuild_/, "")
      elsif gitlab?
        self["object_attributes.status"]
      elsif github_actions?
        self["check_suite.conclusion"]
      else
        nil
      end
    end

    def status_label
      if travis_ci?
        self["status_message"]
      elsif appveyor?
        status
      elsif gitlab?
        self["object_attributes.detailed_status"]
      elsif github_actions?
        self["check_suite.conclusion"]
      else
        nil
      end
    end

    def builds
      if travis_ci?
        @builds ||= builds_travis_ci
      elsif appveyor?
        @builds ||= builds_appveyor
      elsif gitlab?
        @builds ||= builds_gitlab
      elsif github_actions?
        @builds ||= builds_github_actions
      else
        []
      end
    end

    def branch
      if travis_ci?
        if self["pull_request"]
          pull_request_number = self["pull_request_number"]
          "PR\##{pull_request_number}"
        else
          self["branch"]
        end
      elsif appveyor?
        if self["eventData.isPullRequest"]
          pull_request_id = self["eventData.pullRequestId"]
          "PR\##{pull_request_id}"
        else
          self["eventData.branch"]
        end
      elsif gitlab?
        self["object_attributes.ref"]
      elsif github_actions?
        self["check_suite.head_branch"]
      else
        nil
      end
    end

    def commit_author
      if travis_ci?
        self["author_name"]
      elsif appveyor?
        self["eventData.commitAuthor"]
      elsif gitlab?
        self["commit.author.name"]
      elsif github_actions?
        self["check_suite.head_commit.author.name"]
      else
        nil
      end
    end

    def commit_timestamp
      if travis_ci?
        iso8601_value(self["committed_at"])
      elsif appveyor?
        appveyor_time_value(self["eventData.commitDate"])
      elsif gitlab?
        iso8601_value(self["commit.timestamp"])
      elsif github_actions?
        iso8601_value(self["check_suite.head_commit.timestamp"])
      else
        nil
      end
    end

    def commit_id
      if travis_ci?
        self["commit"]
      elsif appveyor?
        self["eventData.commitId"]
      elsif gitlab?
        self["commit.id"]
      elsif github_actions?
        self["check_suite.head_commit.id"]
      else
        nil
      end
    end

    def short_commit_id
      commit_id[0, 8]
    end

    def commit_message
      if travis_ci?
        self["message"]
      elsif appveyor?
        self["eventData.commitMessage"]
      elsif gitlab?
        self["commit.message"]
      elsif github_actions?
        self["check_suite.head_commit.message"]
      else
        nil
      end
    end

    def compare_url
      if travis_ci?
        self["compare_url"]
      elsif appveyor?
        self["eventData.commitUrl"]
      elsif gitlab?
        project_web_url = self["project.web_url"]
        before = self["object_attributes.before_sha"][0, 12]
        after = self["object_attributes.sha"][0, 12]
        "#{project_web_url}/compare/#{before}...#{after}"
      elsif github_actions?
        commit = self["check_suite.head_sha"]
        self["repository.html_url"] + "/commit/#{commit}"
      else
        nil
      end
    end

    def change
      if gitlab_wiki?
        change_gitlab_wiki
      elsif github_gollum?
        change_github_gollumn
      else
        change_push
      end
    end

    private
    def time_value(value)
      if value
        Time.parse(value)
      else
        value
      end
    end

    def appveyor_time_value(value)
      if value
        time = Time.strptime(value, "%m/%d/%Y %H:%M %P")
        Time.utc(*time.to_a)
      else
        value
      end
    end

    def iso8601_value(value)
      if value
        Time.iso8601(value)
      else
        value
      end
    end

    def stringify_hash_keys(hash)
      stringified_hash = {}
      hash.each do |key, value|
        value = stringify_hash_keys(value) if value.is_a?(Hash)
        stringified_hash[key.to_s] = value
      end
      stringified_hash
    end

    def build_conditions_travis_ci(matrix)
      conditions = {}
      config = matrix["config"] || {}
      [
        ["OS", config["os"]],
        ["Language", config["language"]],
        ["Env", config["env"]],
        ["RVM", config["rvm"]],
        ["Gemfile", config["gemfile"]],
        ["Compiler", config["compiler"]],
      ].each do |label, value|
        next if value.nil?
        conditions[label] = value
      end
      conditions
    end

    def builds_travis_ci
      self["matrix"].collect do |matrix|
        build = matrix.dup
        matrix["status"] = matrix["state"]
        config = matrix["config"]
        if config["name"]
          matrix["name"] = config["name"]
          matrix["conditions"] = {}
        else
          matrix["name"] = matrix["number"]
          matrix["conditions"] = build_conditions_travis_ci(matrix)
        end
        id = matrix["id"]
        repository = "#{repository_owner}/#{repository_name}"
        matrix["web_url"] = "https://travis-ci.org/#{repository}/jobs/#{id}"
        started_at = iso8601_value(matrix["started_at"])
        finished_at = iso8601_value(matrix["finished_at"])
        matrix["elapsed_time"] = finished_at - started_at
        matrix["status_label"] = matrix["status"]
        matrix
      end
    end

    def build_conditions_appveyor(job)
      conditions = {}
      job["name"].split("; ").each do |condition|
        label, value = condition.split(": ", 2)
        value = value.strip
        case label
        when "Environment"
          value = value.split(/, /)
        end
        next if value.empty?
        conditions[label] = value
      end
      conditions
    end

    def builds_appveyor
      self["eventData.jobs"].collect do |job|
        job = job.dup
        job["conditions"] = build_conditions_appveyor(job)
        id = job["id"]
        job["web_url"] = "#{web_url}/job/#{id}"
        hours, minutes, seconds = job["duration"].split(":")
        job["elapsed_time"] =
          Integer(hours, 10) * 3600 +
          Integer(minutes, 10) * 60 +
          Float(seconds)
        job["status_label"] = job["status"]
        job["status"] = job["passed"] ? "success" : "failed"
        job
      end
    end

    def builds_gitlab
      self["builds"].collect do |build|
        build = build.dup
        build["conditions"] = build_conditions_gitlab_ci(build)
        id = build["id"]
        project_web_url = self["project.web_url"]
        build["web_url"] = "#{project_web_url}/-/jobs/#{id}"
        started_at = time_value(build["started_at"])
        finished_at = time_value(build["finished_at"])
        if finished_at and started_at
          elapsed_time = finished_at - started_at
        else
          elapsed_time = 0
        end
        build["elapsed_time"] = elapsed_time
        build["status_label"] = build["status"]
        build
      end
    end

    def build_conditions_gitlab_ci(build)
      {}
    end

    def collect_check_runs
      client = Octokit::Client.new
      client.auto_paginate = true
      runs = client.check_runs_for_check_suite(self["repository.full_name"],
                                               self["check_suite.id"])
      runs.check_runs.collect do |run|
        stringify_hash_keys(run.to_h)
      end
    end

    def builds_github_actions
      @check_runs ||= collect_check_runs
      @check_runs.collect do |run|
        build = run
        build["allow_failure"] = false
        build["status"] = build["conclusion"]
        build["status_label"] = build["status"]
        build["web_url"] = build["html_url"]
        build["elapsed_time"] = build["completed_at"] - build["started_at"]
        build["conditions"] = {}
        build
      end
    end

    def change_gitlab_wiki
      before = "HEAD~"
      after = "HEAD"
      reference = "refs/heads/master"
      [before, after, reference]
    end

    def change_github_gollumn
      pages = self["pages"]
      raise UnexpectedPayloadError.new("pages are missing", self) if pages.nil?
      raise UnexpectedPayloadError.new("no pages", self) if pages.empty?

      revisions = pages.collect do |page|
        page["sha"]
      end

      if revisions.size == 1
        after = revisions.first
        before = "#{after}^"
      else
        before = revisions.first
        after = revisions.last
      end

      reference = "refs/heads/master"
      [before, after, reference]
    end

    def change_push
      before = self["before"]
      if before.nil?
        raise UnexpectedPayloadError.new("before commit ID is missing", self)
      end
      after = self["after"]
      if after.nil?
        raise UnexpectedPayloadError.new("after commit ID is missing", self)
      end
      reference = self["ref"]
      if reference.nil?
        raise UnexpectedPayloadError.new("reference is missing", self)
      end

      [before, after, reference]
    end
  end
end
